# Text_Summarization_using_Tensorflow
Model are trained on news article specifically using Encoder-Decoder Architecture and RNN with Tensorflow. Our objective here is to generate a summary for the News articles using the abstraction-based approach 
## Prerequisites
- Tensorflow
- textrank
- nltk
- numpy
- pandas 
- langdetect

## Datasets
Trained on News articles

All three of them are available on Kaggle:
- https://www.kaggle.com/snapcrack/all-the-news

## Code 
The .py files contain the network implementation and utilities. The notebooks are demos of how to apply the model. Maybe it is useful for someone.

## Architecture
**Seq2Seq model**
- Encoder-Decoder
- Bidirectional RNN
- exponential or cyclic learning rate
- Beam Search or Greedy Decoding



